## Run Project
### Activate virtual environment
1. Create
```
python3 -m venv myvenv
```
2. Activate
```
source myvenv/bin/activate
```
### Install library
```
pip install -r /path/to/requirements.txt
```
### Run
```
cd path/manage.py
python manage.py runserver
```

### Run elastic search
```
./manage.py search_index --rebuild
```
