from django.db import models

# Create your models here.


class product(models.Model):
    id = models.AutoField(primary_key=True)
    category_name = models.CharField(max_length=51, null=False)
    product_name = models.CharField(max_length=249, null=False)
    brand_id = models.IntegerField(null=False)
    brand_name = models.CharField(max_length=23, null=False)
    short_description = models.CharField(max_length=203, null=False)
    price = models.IntegerField(null=False)
    list_price = models.IntegerField(null=False)
    discount = models.IntegerField(null=False)
    discount_rate = models.IntegerField(null=False)
    rating_avg = models.DecimalField(max_digits=3, decimal_places=1, null=False)
    review_count = models.IntegerField(null=False)
    inventory_status = models.CharField(max_length=9, null=False)
    image = models.CharField(max_length=156, null=False, default='default_image.jpg')
