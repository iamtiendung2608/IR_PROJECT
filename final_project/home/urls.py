from django.urls import path
from . import views

urlpatterns = [
    # path('', views.homepageView, name='home'),
    # path('input/', views.input_view, name='input'),
    path('search', views.search, name='search'),
    path('search/<int:id>', views.detail, name='details'),
    path('search_product', views.search_product, name='search_product'),
]