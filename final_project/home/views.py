from django.shortcuts import render
from django.shortcuts import redirect
from django.views.generic import ListView
from elasticsearch_dsl import Q, Search
from elasticsearch_dsl import Index
import json
from elasticsearch import Elasticsearch
from . import models
from . import document
from elasticsearch.helpers import bulk
from decimal import Decimal
from elasticsearch_dsl import Range
import sys
from django.core.paginator import Paginator



client = Elasticsearch()

LIMIT = 4
SIZE_LIMIT = 10
def search(request):
    brands = models.product.objects.values("brand_name").distinct()
    categories = models.product.objects.values("category_name").distinct()[:20]
    context = {'brands' : brands, 'categories' : categories}
    category_name = request.GET.get('category_name')
    if request.method == 'GET':
        page = request.GET.get('page', 0)
        query = Q('bool')
        if category_name:
            query.must.append(Q('match', category_name=category_name))
        OFFSET = SIZE_LIMIT * int(page)
        products = document.ProductDocument.search().query(query).sort('-review_count')[OFFSET:OFFSET + SIZE_LIMIT]
        context['products'] = products
        context['page'] = page
        print(products.to_dict())
        return render(request, 'search.html', context)
    elif request.method == 'POST':
        sort = request.POST.get('sort')
        keyword = request.POST.get('keyword')
        brand_name = request.POST.get('brand_name')
        min_price = request.POST.get('min_price')
        max_price = request.POST.get('max_price')
        context['min_price'] = min_price
        context['max_price'] = max_price
        query = Q('bool')
        page = request.POST.get('page', 0)
        OFFSET = SIZE_LIMIT * int(page)
        if brand_name:
            query.must.append(Q('match', brand_name=brand_name))

        if min_price is None or min_price == "":
            min_price = 0

        if max_price is None or max_price == "":
            max_price = 75990000

        query.must.append(Q('range', price={'gte': min_price, 'lte': max_price}))
        if keyword:
            query.must.append(Q('wildcard', product_name={'value': f'*{keyword.lower()}*', 'boost': 1}))

        if category_name:
            query.must.append(Q('match', category_name=category_name))

        sort_query = '-review_count'
        if sort == 'asc':
            sort_query = 'price'
        elif sort == 'desc':
            sort_query = '-price'
        products = document.ProductDocument.search().query(query).sort(sort_query)[OFFSET:OFFSET + SIZE_LIMIT]
        print(products.to_dict())
        context['products'] = products
        context['brand_name'] = brand_name
        context['keyword'] = keyword
        context['sort'] = sort
        return render(request, 'search.html', context)


def detail(request, id = None):
    if id is None:
        return redirect('search')
    product = document.ProductDocument.get(id = id)
    context = {'product': product}
    relateQuery = Q('bool', should=[
            Q('match', category_name=product.category_name) if product.category_name else Q(),
            Q('match', brand_name=product.brand_name) if product.brand_name else Q()
        ])
    relateQuery &= ~Q('term', id=id)
    relateProducts = document.ProductDocument.search().query(relateQuery).extra(size = LIMIT)
    context['relates'] = relateProducts
    return render(request, 'details.html', context)

def search_product(request):
    brands = models.product.objects.values("brand_name").distinct()
    categories = models.product.objects.values("category_name").distinct()[:20]
    context = {'brands' : brands, 'categories' : categories}
    page = request.GET.get('page', 0)
    OFFSET = SIZE_LIMIT * int(page)
    if request.method == 'GET':
        category_name = request.GET.get('category_name')
        if category_name:
            products = document.ProductDocument.search().query("match", category_name=category_name)[OFFSET:OFFSET + SIZE_LIMIT]
        else:
            products = []
        context['products'] = products
        return render(request, 'search.html', context)

