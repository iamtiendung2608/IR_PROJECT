from django.shortcuts import render
from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search
from . import documents
# Create your views here.
client = Elasticsearch()

def input_view(request):
    s = documents.CarDocument.search().filter("term", color="red")
    s = documents.CarDocument.search().query("match", description="beautiful")
    for hit in s:
        print(
            "Car name : {}, description {}".format(hit.name, hit.description)
        )
